class Comment < ActiveRecord::Base

	# relations
	belongs_to :post

	# constraints
	validates_presence_of :body
	validates_presence_of :post_id

end
