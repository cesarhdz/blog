class Post < ActiveRecord::Base

	# Relations
	has_many :comments, dependent: :destroy

	# constraints
	validates_presence_of :title
	validates_presence_of :body

end
